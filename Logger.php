<?php

class Logger{

    private function __construct() {}
    private function __clone() {}

    public static function log_msg($message) {
        if (is_array($message)) {
            $message = json_encode($message);
        }

        Logger::log_write('[INFO] ' . $message);
    }

    public static function log_debug($message) {
        if (is_array($message)) {
            $message = json_encode($message);
        }

        Logger::log_write('[DEBUG] ' . $message);
    }

    public static function log_error($message) {
        if (is_array($message)) {
            $message = json_encode($message);
        }

        Logger::log_write('[ERROR] ' . $message);
    }

    public static function log_write($message) {
        $trace = debug_backtrace();
        $function_name = isset($trace[2]) ? $trace[2]['function'] : '-';
        $mark = date("H:i:s") . ' [' . $function_name . ']';
        $log_name = BOT_LOGS_DIRECTORY.'/log_' . date("j.n.Y") . '.txt';
        file_put_contents($log_name, $mark . " : " . $message . "\n", FILE_APPEND);
    }

}
